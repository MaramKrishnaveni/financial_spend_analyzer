package com.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.entity.TransactionsMonthly;

@Repository
public interface TransactionMonthlyRepository extends JpaRepository<TransactionsMonthly, Integer> {
	public List<TransactionsMonthly> findAllByAccountNumber(Long accountNumber, Pageable pageable);
}
