package com.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.entity.Account;

public interface AcountRepository extends JpaRepository<Account, Integer> {

	public Optional<Account> findByAccountNumber(Long accountNumber);

	public Optional<Account> findAllByAccountNumber(Long customerAccountNumber);

}
