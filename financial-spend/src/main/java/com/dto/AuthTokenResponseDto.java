package com.dto;

public class AuthTokenResponseDto {

    private String token;
    private String username;

    public AuthTokenResponseDto(){

    }

    public AuthTokenResponseDto(String token, String username){
        this.token = token;
        this.username = username;
    }

    public AuthTokenResponseDto(String token){
        this.token = token;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
