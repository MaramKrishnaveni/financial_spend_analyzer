package com.controller;

import java.util.List;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.dto.MonthlyTransactionResponseDto;
import com.exception.AccountNotFoundException;
import com.exception.NoTransactionHistoryException;
import com.service.TransactionMonthlyService;

/**
 * This controller is handling the requests and responses for user operation
 * transactions.
 * 
 * @author SQUAD 3
 * @since 2020/11/26
 */
@Validated
@RestController
@RequestMapping("/api/transactions/monthly")
public class TransactionMonthlyController {

	@Autowired
	TransactionMonthlyService transactionMonthlyService;

	/**
	 * Method to call service method for requesting to get past transactions history
	 * 
	 * @param accountNumber
	 * @param pageNumber
	 * @param pagesize
	 * @return Optional<MonthlyTransactionResponseDto> which consist of connection
	 *         month,total income total outcome, closing balance amountNumber
	 * 
	 */
	@GetMapping
	public ResponseEntity<List<MonthlyTransactionResponseDto>> getMonthlyTransaction(
			@RequestParam(name = "accountNumber") @NotNull(message = "Account number required") Long accountNumber,
			@RequestParam(name = "number") @NotNull(message = "page number required") @Min(value = 0, message = "page number cannot be negative integer") Integer pageNumber,
			@RequestParam(name = "size") @NotNull(message = "page size required") @Min(value = 1, message = "page size should be positive integer") Integer pagesize)
			throws AccountNotFoundException, NoTransactionHistoryException {
		Pageable pageable = PageRequest.of(pageNumber, pagesize);
		List<MonthlyTransactionResponseDto> listMonthlyTransactionResponseDto = transactionMonthlyService
				.getMonthlyTransaction(accountNumber, pageable);

		return new ResponseEntity<>(listMonthlyTransactionResponseDto, HttpStatus.OK);
	}

}
