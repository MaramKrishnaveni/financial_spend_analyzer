package com.exception;

import java.util.ArrayList;
import java.util.List;

import javax.validation.ConstraintViolationException;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.dto.ErrorListResponseDto;
import com.dto.ErrorResponseDto;

@RestControllerAdvice
public class RestExceptionHandler extends ResponseEntityExceptionHandler {

	@ExceptionHandler(value = AccountNotFoundException.class)
	public ResponseEntity<ErrorResponseDto> handleAccountNotFoundException(
			AccountNotFoundException accountNotFoundException) {
		ErrorResponseDto errorResponseDto = new ErrorResponseDto();
		errorResponseDto.setStatusCode("error-101");
		errorResponseDto.setStatusMessage(accountNotFoundException.getMessage());
		return new ResponseEntity<>(errorResponseDto, HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(value = NoTransactionHistoryException.class)
	public ResponseEntity<ErrorResponseDto> handleNoTransactionHistoryException(
			NoTransactionHistoryException noTransactionHistoryException) {
		ErrorResponseDto errorResponseDto = new ErrorResponseDto();
		errorResponseDto.setStatusCode("error-102");
		errorResponseDto.setStatusMessage(noTransactionHistoryException.getMessage());
		return new ResponseEntity<>(errorResponseDto, HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(value = ConstraintViolationException.class)
	public ResponseEntity<ErrorResponseDto> handleConstraintViolationException(
			ConstraintViolationException constraintViolationException) {
		ErrorResponseDto errorResponseDto = new ErrorResponseDto();
		errorResponseDto.setStatusCode("error-103");
		errorResponseDto.setStatusMessage(constraintViolationException.getMessage());
		return new ResponseEntity<ErrorResponseDto>(errorResponseDto, HttpStatus.BAD_REQUEST);
	}

	@Override
	protected ResponseEntity<Object> handleMethodArgumentNotValid(
			MethodArgumentNotValidException methodArgumentNotValidException, HttpHeaders headers, HttpStatus status,
			WebRequest request) {
		List<String> errorMessages = new ArrayList<String>();
		for (FieldError error : methodArgumentNotValidException.getBindingResult().getFieldErrors()) {
			errorMessages.add(error.getField() + ": " + error.getDefaultMessage());
		}
		for (ObjectError error : methodArgumentNotValidException.getBindingResult().getGlobalErrors()) {
			errorMessages.add(error.getObjectName() + ": " + error.getDefaultMessage());
		}

		ErrorListResponseDto errorListResponseDto = new ErrorListResponseDto("error-104", errorMessages);
		return handleExceptionInternal(methodArgumentNotValidException, errorListResponseDto, headers,
				HttpStatus.BAD_REQUEST, request);
	}

	@ExceptionHandler(value = UsernameNotFoundException.class)
	public ResponseEntity<ErrorResponseDto> handleUsernameNotFoundException(
			UsernameNotFoundException usernameNotFoundException) {
		ErrorResponseDto errorResponseDto = new ErrorResponseDto();
		errorResponseDto.setStatusCode("error-105");
		errorResponseDto.setStatusMessage(usernameNotFoundException.getMessage());
		return new ResponseEntity<ErrorResponseDto>(errorResponseDto, HttpStatus.BAD_REQUEST);
	}
	
	@ExceptionHandler(UserAccountNotFoundException.class)
	public ResponseEntity<ErrorResponseDto> userAccountNotFound(UserAccountNotFoundException userAccountNotFoundException) {
		ErrorResponseDto errorResponseDto = new ErrorResponseDto();
		errorResponseDto.setStatusCode("error-106");
		errorResponseDto.setStatusMessage(userAccountNotFoundException.getMessage());
		return new ResponseEntity<ErrorResponseDto>(errorResponseDto, HttpStatus.BAD_REQUEST);

	}
	
	@ExceptionHandler(value=TransactionIdNotFoundException.class)
	public ResponseEntity<ErrorResponseDto> transactionIdNotFound(TransactionIdNotFoundException transactionIdNotFoundException) {
		ErrorResponseDto errorResponseDto = new ErrorResponseDto();
		errorResponseDto.setStatusCode("error-107");
		errorResponseDto.setStatusMessage(transactionIdNotFoundException.getMessage());
		return new ResponseEntity<ErrorResponseDto>(errorResponseDto, HttpStatus.BAD_REQUEST);

	}
	
	@ExceptionHandler(InsufficientFundException.class)
	public ResponseEntity<ErrorResponseDto> insufficientFund(InsufficientFundException insufficientFundsInAccount) {
		ErrorResponseDto errorResponseDto = new ErrorResponseDto();
		errorResponseDto.setStatusCode("error-108");
		errorResponseDto.setStatusMessage(insufficientFundsInAccount.getMessage());
		return new ResponseEntity<ErrorResponseDto>(errorResponseDto, HttpStatus.BAD_REQUEST);
	}
	
}
