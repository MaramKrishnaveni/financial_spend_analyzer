package com.utils;

public class GlobalConstants {
 
	public GlobalConstants() {
	 
 }
	

public static final String USER_ACCOUNTNUMBER_NOT_FOUND="user account number is not found";
public static final String TRANSACTION_STATUS_MESSAGE="transaction done succesfull";
public static final String STATUS_MESSAGE="success";
public static final String TRANSACTIONID_NOT_FOUND="transaction id number is not found";
public static final String PAYMENT_TYPE="credit";
public static final String INSUFFICIENT_FUND="insuffucient funds in your account";

public static final long ACCESS_TOKEN_VALIDITY_SECONDS = 5*60*60;
public static final String SIGNING_KEY = "santhosh123r";
public static final String TOKEN_PREFIX = "Bearer ";
public static final String HEADER_STRING = "Authorization";

}