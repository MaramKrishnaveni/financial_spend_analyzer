package com;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FinancialSpendApplication {

	public static void main(String[] args) {
		SpringApplication.run(FinancialSpendApplication.class);
	}

}
