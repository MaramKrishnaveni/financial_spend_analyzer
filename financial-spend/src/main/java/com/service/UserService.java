package com.service;

import java.util.List;
import java.util.Optional;

import com.dto.TransactionsDto;
import com.dto.UserDto;
import com.entity.User;
import com.exception.UserAccountNotFoundException;

/**
 * TransactionMonthlyService
 * 
 * @author SQUAD 3
 * @since 2020/11/26
 */
public interface UserService {

	User save(UserDto user);

	List<User> findAll();

	void delete(int id);

	User findOne(String username);

	User findById(int id);

	/**
	 * user can get past transactions history
	 * 
	 * @param accountNumber
	 * @return Optional<TransactionsDto> which consist of connection
	 *         userId,transactionDescription,amount,payment type,date
	 * 
	 */
	Optional<List<TransactionsDto>> showAllTransactionData(Long accountNumber) throws UserAccountNotFoundException;
}
