package com.service;

import java.util.List;

import org.springframework.data.domain.Pageable;

import com.dto.MonthlyTransactionResponseDto;
import com.exception.AccountNotFoundException;
import com.exception.NoTransactionHistoryException;

/**
 * TransactionMonthlyService
 * 
 * @author SQUAD 3
 * @since 2020/11/26
 */
public interface TransactionMonthlyService {

	/**
	 * user can get the monthly transactions
	 * 
	 * @param accountNumber
	 * @param pageNumber
	 * @param pagesize
	 * @return Optional<MonthlyTransactionResponseDto> which consist of connection
	 *         month,total income total outcome, closing balance amountNumber
	 * 
	 */
	public List<MonthlyTransactionResponseDto> getMonthlyTransaction(Long accountNumber, Pageable pageable)
			throws AccountNotFoundException, NoTransactionHistoryException;
}
