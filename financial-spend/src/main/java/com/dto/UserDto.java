package com.dto;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

public class UserDto {

	@NotNull(message = "first name should not be null")
	@NotEmpty(message = "first name should not be empty")
	private String firstName;
	@NotNull(message = "last name should not be null")
	@NotEmpty(message = "last name should not be empty")
	private String lastName;
	@NotNull(message = "user name should not be null")
	@NotEmpty(message = "user name should not be empty")
	private String username;
	@NotNull(message = "password should not be null")
	@NotEmpty(message = "password should not be empty")
	private String password;

	@NotNull(message = "account number should not  null")
	private Long accountNumber;

	@NotNull(message = "address number should not  null")
	@NotEmpty(message = "address number should not empty")
	private String address;

	public Long getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(Long accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
}
