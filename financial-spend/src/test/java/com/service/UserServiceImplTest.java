package com.service;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import com.dto.TransactionsDto;
import com.entity.Transactions;
import com.entity.User;
import com.exception.UserAccountNotFoundException;
import com.repository.TransactionsRepository;
import com.repository.UserRepository;

@ExtendWith(MockitoExtension.class)
@TestInstance(Lifecycle.PER_CLASS)
class UserServiceImplTest {

	@Mock
	UserRepository userRepository;

	@Mock
	TransactionsRepository transactionRepository;

	@InjectMocks
	UserServiceImpl userServiceImpl;
	private final Long accountNumber = 123456L;
	User users;
	Transactions transactions;
	List<Transactions> listOfTransactions;

	@BeforeAll
	public void setup() {
		users = new User();
		users.setAccountNumber(123456L);
		users.setAddress("ong");
		users.setId(1);
		users.setFirstName("maram");
		users.setLastName("marmas");
		users.setUsername("krishnaveni");
		users.setPassword("maram123");

		listOfTransactions = new ArrayList<>();
		transactions = new Transactions();
		transactions.setAccountNumber(123456L);
		transactions.setAmount(2000.0);
		transactions.setDate(LocalDate.parse("2020-11-25"));
		transactions.setPaymentType("credit");

		transactions.setTransactionDesc("initial");
		transactions.setTransactionsId(1);
		listOfTransactions.add(transactions);
	}

	@Test
	void getPastTransactionsTest() throws UserAccountNotFoundException {
		Mockito.when(userRepository.findByAccountNumber(accountNumber)).thenReturn(Optional.of(users));
		Mockito.when(transactionRepository.findAllByAccountNumber(accountNumber))
				.thenReturn(Optional.of(listOfTransactions));
		Optional<List<TransactionsDto>> result = userServiceImpl.showAllTransactionData(accountNumber);
		assertNotNull(result);
	}
}
