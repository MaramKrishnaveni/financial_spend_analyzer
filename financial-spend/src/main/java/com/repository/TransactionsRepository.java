package com.repository;



import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.entity.Transactions;

@Repository
public interface TransactionsRepository extends JpaRepository<Transactions, Integer>{



	Optional<List<Transactions>> findAllByAccountNumber(Long accountNumber);

}
