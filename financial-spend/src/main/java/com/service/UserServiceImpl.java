package com.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.dto.TransactionsDto;
import com.dto.UserDto;
import com.entity.Transactions;
import com.entity.User;
import com.exception.UserAccountNotFoundException;
import com.repository.TransactionsRepository;
import com.repository.UserRepository;
import com.utils.GlobalConstants;

/**
 * Implementation of UserService which will do the user requested operations.
 * 
 * @author SQUAD 3
 * @since 2020/11/26
 */
@Service(value = "userService")
public class UserServiceImpl implements UserDetailsService, UserService {

	@Autowired
	private UserRepository userRepository;

	@Autowired
	TransactionsRepository transactionsRepository;

	@Autowired
	private BCryptPasswordEncoder bcryptEncoder;

	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		User user = userRepository.findByUsername(username);
		if (user == null) {
			throw new UsernameNotFoundException("Invalid username or password.");
		}
		return new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(),
				getAuthority());
	}

	private List<SimpleGrantedAuthority> getAuthority() {
		return Arrays.asList(new SimpleGrantedAuthority("ROLE_ADMIN"));
	}

	public List<User> findAll() {
		List<User> list = new ArrayList<>();
		userRepository.findAll().iterator().forEachRemaining(list::add);
		return list;
	}

	@Override
	public void delete(int id) {
		userRepository.deleteById(id);
	}

	@Override
	public User findOne(String username) {
		return userRepository.findByUsername(username);
	}

	@Override
	public User findById(int id) {
		Optional<User> optionalUser = userRepository.findById(id);
		return optionalUser.isPresent() ? optionalUser.get() : null;
	}

	@Override
	public User save(UserDto user) {
		User newUser = new User();
		newUser.setUsername(user.getUsername());
		newUser.setFirstName(user.getFirstName());
		newUser.setLastName(user.getLastName());
		newUser.setAccountNumber(user.getAccountNumber());
		newUser.setAddress(user.getAddress());
		newUser.setPassword(bcryptEncoder.encode(user.getPassword()));
		return userRepository.save(newUser);
	}

	/**
	 * method for requesting to get past transactions history
	 * 
	 * @param accountNumber
	 * @return Optional<TransactionsDto> which consist of connection
	 *         userId,transactionDescription,amount,payment type,date
	 * 
	 */
	@Override
	public Optional<List<TransactionsDto>> showAllTransactionData(Long accountNumber)
			throws UserAccountNotFoundException {

		Optional<User> userAccountNo = userRepository.findByAccountNumber(accountNumber);

		if (!userAccountNo.isPresent()) {
			throw new UserAccountNotFoundException(GlobalConstants.USER_ACCOUNTNUMBER_NOT_FOUND);
		}

		Optional<List<Transactions>> transactionAccNumber = transactionsRepository
				.findAllByAccountNumber(userAccountNo.get().getAccountNumber());

		List<TransactionsDto> listOfTransactionDto = new ArrayList<>();

		transactionAccNumber.get().forEach(history -> {
			TransactionsDto transactionsDto = new TransactionsDto();
			transactionsDto.setAmount(history.getAmount());
			transactionsDto.setUserId(userAccountNo.get().getId());
			transactionsDto.setDate(String.valueOf(history.getDate()));
			transactionsDto.setPaymentType(history.getPaymentType());
			transactionsDto.setTransactionDescription(history.getTransactionDesc());

			listOfTransactionDto.add(transactionsDto);

		});

		return Optional.of(listOfTransactionDto);

	}
}
