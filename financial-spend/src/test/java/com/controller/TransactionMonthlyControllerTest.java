package com.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.dto.MonthlyTransactionResponseDto;
import com.exception.AccountNotFoundException;
import com.exception.NoTransactionHistoryException;
import com.service.TransactionMonthlyService;

@ExtendWith(MockitoExtension.class)
public class TransactionMonthlyControllerTest {
	@Mock
	TransactionMonthlyService transactionMonthlyService;

	@InjectMocks
	TransactionMonthlyController transactionMonthlyController;

	static Pageable pageable;
	static List<MonthlyTransactionResponseDto> listMonthlyTransactionResponseDto;
	// static UserResponseDto userResponse;

	@BeforeAll
	public static void setUp() {
		pageable = PageRequest.of(0, 5);
		listMonthlyTransactionResponseDto = new ArrayList<>();
		MonthlyTransactionResponseDto monthlyTransactionResponseDto1 = new MonthlyTransactionResponseDto();
		monthlyTransactionResponseDto1.setAccountNumber((long) 45785456);
		monthlyTransactionResponseDto1.setClosingBalance(1250);
		monthlyTransactionResponseDto1.setTotalIncome(0);
		monthlyTransactionResponseDto1.setTotalOutcome(50);
		monthlyTransactionResponseDto1.setMonth(LocalDate.of(2020, 1, 1));
		listMonthlyTransactionResponseDto.add(monthlyTransactionResponseDto1);

		MonthlyTransactionResponseDto monthlyTransactionResponseDto2 = new MonthlyTransactionResponseDto();
		monthlyTransactionResponseDto2.setAccountNumber((long) 55785455);
		monthlyTransactionResponseDto2.setClosingBalance(1000);
		monthlyTransactionResponseDto2.setTotalIncome(70);
		monthlyTransactionResponseDto2.setTotalOutcome(50);
		monthlyTransactionResponseDto2.setMonth(LocalDate.of(2020, 1, 1));
		listMonthlyTransactionResponseDto.add(monthlyTransactionResponseDto2);
	}

	@Test
	@DisplayName("Get monthly transaction: Positive Scenario")
	public void getMonthlyTransactionTest() throws AccountNotFoundException, NoTransactionHistoryException {
		Long accountNumber = new Long(45785456);
		when(transactionMonthlyService.getMonthlyTransaction(accountNumber, pageable))
				.thenReturn(listMonthlyTransactionResponseDto);

		ResponseEntity<List<MonthlyTransactionResponseDto>> result = transactionMonthlyController
				.getMonthlyTransaction(accountNumber, 0, 5);

		verify(transactionMonthlyService).getMonthlyTransaction(accountNumber, pageable);
		assertEquals(2, result.getBody().size());
		assertEquals(HttpStatus.OK, result.getStatusCode());
	}

	@Test
	@DisplayName("Get monthly transaction: Negative Scenario")
	public void userNewConnectionNegativeTest() {

	}
}
