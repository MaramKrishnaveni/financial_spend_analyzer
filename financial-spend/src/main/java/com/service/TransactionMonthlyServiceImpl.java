package com.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.dto.MonthlyTransactionResponseDto;
import com.entity.Account;
import com.entity.TransactionsMonthly;
import com.exception.AccountNotFoundException;
import com.exception.NoTransactionHistoryException;
import com.repository.AcountRepository;
import com.repository.TransactionMonthlyRepository;

/**
 * Implementation of TransactionMonthlyService which will do the user request
 * related transactions i.e to get past three months statement.
 * 
 * @author SQUAD 3
 * @since 2020/11/26
 */
@Service
public class TransactionMonthlyServiceImpl implements TransactionMonthlyService {
	@Autowired
	AcountRepository acountRepository;

	@Autowired
	TransactionMonthlyRepository transactionMonthlyRepository;

	/**
	 * Method to call service method for requesting to get past transactions history
	 * 
	 * @param accountNumber
	 * @param pageNumber
	 * @param pagesize
	 * @return Optional<MonthlyTransactionResponseDto> which consist of connection
	 *         month,total income total outcome, closing balance amountNumber
	 * 
	 */
	@Override
	public List<MonthlyTransactionResponseDto> getMonthlyTransaction(Long accountNumber, Pageable pageable)
			throws AccountNotFoundException, NoTransactionHistoryException {
		Optional<Account> optionalAccount = acountRepository.findByAccountNumber(accountNumber);
		if (!optionalAccount.isPresent())
			throw new AccountNotFoundException("account doesnot exist with account number " + accountNumber);

		List<TransactionsMonthly> listTransactionMonthly = transactionMonthlyRepository
				.findAllByAccountNumber(accountNumber, pageable);

		if (listTransactionMonthly.isEmpty())
			throw new NoTransactionHistoryException("No transaction history exist for given account " + accountNumber);

		List<MonthlyTransactionResponseDto> listMonthlyTransactionResponseDto = new ArrayList<>();
		listTransactionMonthly.stream().forEach(transactionsMonthly -> {
			MonthlyTransactionResponseDto monthlyTransactionResponseDto = new MonthlyTransactionResponseDto();
			BeanUtils.copyProperties(transactionsMonthly, monthlyTransactionResponseDto);
			listMonthlyTransactionResponseDto.add(monthlyTransactionResponseDto);
		});
		return listMonthlyTransactionResponseDto;
	}
}
