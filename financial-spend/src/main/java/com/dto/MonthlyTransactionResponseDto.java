package com.dto;

import java.time.LocalDate;
import java.time.format.TextStyle;
import java.util.Locale;

public class MonthlyTransactionResponseDto {
	private String month;
	private Long accountNumber;
	private float totalIncome;
	private float totalOutcome;
	private float ClosingBalance;

	public String getMonth() {
		return month;
	}

	public void setMonth(LocalDate month) {
		this.month = month.getMonth().getDisplayName(TextStyle.SHORT, Locale.US) + "-" + month.getYear();
	}

	public Long getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(Long accountNumber) {
		this.accountNumber = accountNumber;
	}

	public float getTotalIncome() {
		return totalIncome;
	}

	public void setTotalIncome(float totalIncome) {
		this.totalIncome = totalIncome;
	}

	public float getTotalOutcome() {
		return totalOutcome;
	}

	public void setTotalOutcome(float totalOutcome) {
		this.totalOutcome = totalOutcome;
	}

	public float getClosingBalance() {
		return ClosingBalance;
	}

	public void setClosingBalance(float closingBalance) {
		ClosingBalance = closingBalance;
	}
}
