package com.service;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.time.LocalDate;
import java.util.Optional;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;

import com.dto.ResponseDto;
import com.dto.TransactionsDto;
import com.entity.Account;
import com.entity.User;
import com.exception.InsufficientFundException;
import com.exception.UserAccountNotFoundException;
import com.repository.AcountRepository;
import com.repository.TransactionsRepository;
import com.repository.UserRepository;

@ExtendWith(MockitoExtension.class)
@TestInstance(Lifecycle.PER_CLASS)
class TransactionsServiceImplTest {

	@Mock
	UserRepository userRepository;

	@Mock
	TransactionsRepository transactionRepository;
	@Mock
	AcountRepository accountRepository;
	@InjectMocks
	TransactionServiceImpl transactionServiceImpl;
	User users;
	Account account;
	TransactionsDto transactionDto;
	ResponseDto responseDto;
	private final Long accountNumber = 123456L;

	@BeforeAll
	public void setup() {
		users = new User();
		users.setAccountNumber(123456L);
		users.setAddress("ong");
		users.setId(1);
		users.setFirstName("maram");
		users.setLastName("marmas");
		users.setUsername("krishnaveni");
		users.setPassword("maram123");

		account = new Account();
		account.setAccountId(1);
		account.setAccountNumber(123456L);
		account.setClosingBalance((float) 10000);
		account.setOpeningBalance((float) 5000);
		account.setOpeningDate(LocalDate.parse("2020-11-25"));

		transactionDto = new TransactionsDto();

		transactionDto.setAmount(2000.0);
		transactionDto.setUserId(1);
		transactionDto.setDate("2020-11-14");
		transactionDto.setPaymentType("credit");
		transactionDto.setTransactionDescription("initial transaction");

		responseDto = new ResponseDto();
		responseDto.setMessage("success");
		responseDto.setStatus(HttpStatus.OK.value());
		responseDto.setTransactionMessage("transaction done succesfull");
	}

	@Test
	void makePaymentAndRecieveTest() throws InsufficientFundException, UserAccountNotFoundException {

		Mockito.when(userRepository.findById(1)).thenReturn(Optional.of(users));
		Mockito.when(accountRepository.findAllByAccountNumber(accountNumber)).thenReturn(Optional.of(account));

		Optional<ResponseDto> result = transactionServiceImpl.makePaymentAndRecieve(transactionDto);
		assertEquals("transaction done succesfull", result.get().getTransactionMessage());
	}

}
