package com.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Optional;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.dto.ResponseDto;
import com.dto.TransactionsDto;
import com.exception.InsufficientFundException;
import com.exception.UserAccountNotFoundException;
import com.service.TransactionService;

@ExtendWith(MockitoExtension.class)
@TestInstance(Lifecycle.PER_CLASS)
class TransactionControllerTest {

	@Mock
	TransactionService transactionService;

	@InjectMocks
	TransactionController transactionController;
	TransactionsDto transactionDto;
	ResponseDto responseDto;

	@BeforeAll
	public void setup() {

		transactionDto = new TransactionsDto();
		transactionDto.setAmount(2000.0);
		transactionDto.setUserId(2);
		transactionDto.setDate("2020-11-14");
		transactionDto.setPaymentType("credit");
		transactionDto.setTransactionDescription("initial transaction");

		responseDto = new ResponseDto();
		responseDto.setMessage("success");
		responseDto.setStatus(HttpStatus.OK.value());
		responseDto.setTransactionMessage("transaction done succesfull");

	}

	@Test
	void makePaymentAndRecievePaymentTest() throws InsufficientFundException, UserAccountNotFoundException {
		Mockito.when(transactionService.makePaymentAndRecieve(transactionDto)).thenReturn(Optional.of(responseDto));
		ResponseEntity<Optional<ResponseDto>> result = transactionController.makeATransactions(transactionDto);
		assertEquals("transaction done succesfull", result.getBody().get().getTransactionMessage());
	}
}
