package com.entity;

import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "transactions_monthly")
public class TransactionsMonthly {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer transactionsMonthlyId;
	private LocalDate month;
	private Long accountNumber;
	private float totalIncome;
	private float totalOutcome;
	private float ClosingBalance;

	public Integer getTransactionsMonthlyId() {
		return transactionsMonthlyId;
	}

	public void setTransactionsMonthlyId(Integer transactionsMonthlyId) {
		this.transactionsMonthlyId = transactionsMonthlyId;
	}

	public LocalDate getMonth() {
		return month;
	}

	public void setMonth(LocalDate month) {
		this.month = month;
	}

	public Long getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(Long accountNumber) {
		this.accountNumber = accountNumber;
	}

	public float getTotalIncome() {
		return totalIncome;
	}

	public void setTotalIncome(float totalIncome) {
		this.totalIncome = totalIncome;
	}

	public float getTotalOutcome() {
		return totalOutcome;
	}

	public void setTotalOutcome(float totalOutcome) {
		this.totalOutcome = totalOutcome;
	}

	public float getClosingBalance() {
		return ClosingBalance;
	}

	public void setClosingBalance(float closingBalance) {
		ClosingBalance = closingBalance;
	}
}
