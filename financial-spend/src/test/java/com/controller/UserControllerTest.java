package com.controller;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.ResponseEntity;

import com.dto.TransactionsDto;
import com.exception.UserAccountNotFoundException;
import com.service.UserService;

@ExtendWith(MockitoExtension.class)
@TestInstance(Lifecycle.PER_CLASS)
class UserControllerTest {

	@Mock
	UserService userService;

	@InjectMocks
	UserController userController;
	private final Long accountNumber = 123456L;

	List<TransactionsDto> transactions;

	TransactionsDto transactionDto;

	@BeforeAll
	public void setup() {

		transactionDto = new TransactionsDto();
		transactions = new ArrayList<>();
		transactionDto.setAmount(2000.0);
		transactionDto.setUserId(1);
		transactionDto.setDate("2020-11-14");
		transactionDto.setPaymentType("credit");
		transactionDto.setTransactionDescription("initial transaction");
		transactions.add(transactionDto);

	}

	@Test
	void getPastTransactionsHistoryTest() throws UserAccountNotFoundException {
		Mockito.when(userService.showAllTransactionData(accountNumber)).thenReturn(Optional.of(transactions));
		ResponseEntity<Optional<List<TransactionsDto>>> result = userController.showPastTransactions(accountNumber);
		assertNotNull(result);

	}

}
