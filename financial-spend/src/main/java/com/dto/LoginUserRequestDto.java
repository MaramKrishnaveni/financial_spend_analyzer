package com.dto;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

public class LoginUserRequestDto {

	@NotNull(message = "user name should not be null")
	@NotEmpty(message = "user name should not be empty")
	private String username;
	@NotNull(message = "password should not be null")
	@NotEmpty(message = "password should not be empty")
	private String password;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
}
