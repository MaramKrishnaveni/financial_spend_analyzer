package com.service;

import java.time.LocalDate;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.dto.ResponseDto;
import com.dto.TransactionsDto;
import com.entity.Account;
import com.entity.Transactions;
import com.entity.User;
import com.exception.InsufficientFundException;
import com.exception.UserAccountNotFoundException;
import com.repository.AcountRepository;
import com.repository.TransactionsRepository;
import com.repository.UserRepository;
import com.utils.GlobalConstants;

/**
 * Implementation of TransactionService which will do the user request related
 * transactions i.e make payment or receive payment.
 * 
 * @author SQUAD 3
 * @since 2020/11/26
 */
@Service
public class TransactionServiceImpl implements TransactionService {

	@Autowired
	TransactionsRepository transactionsRepository;

	@Autowired
	AcountRepository accountRepository;

	@Autowired
	UserRepository userRepository;

	/**
	 * method for user to make transactions
	 * 
	 * @RequestBody transactionsDto
	 * @return Optional<ResponseDto> which consist of status message, status code,
	 *         Transaction message
	 * 
	 */
	@Override
	@Transactional
	public Optional<ResponseDto> makePaymentAndRecieve(TransactionsDto transactionsDto)
			throws InsufficientFundException, UserAccountNotFoundException {

		Optional<User> customerIdPresent = userRepository.findById(transactionsDto.getUserId());

		Long customerAccountNumber = customerIdPresent.get().getAccountNumber();

		Optional<Account> accountNumberPresent = accountRepository.findAllByAccountNumber(customerAccountNumber);

		if (!accountNumberPresent.isPresent()) {
			throw new UserAccountNotFoundException(GlobalConstants.USER_ACCOUNTNUMBER_NOT_FOUND);
		}

		if (accountNumberPresent.get().getClosingBalance() > transactionsDto.getAmount()) {

			TransactionsDto dto = new TransactionsDto();

			Transactions transactions = new Transactions();
			BeanUtils.copyProperties(dto, transactions);

			transactions.setAmount(transactionsDto.getAmount());
			transactions.setAccountNumber(accountNumberPresent.get().getAccountNumber());

			LocalDate date = LocalDate.parse(transactionsDto.getDate());
			transactions.setDate(date);

			transactions.setPaymentType(transactionsDto.getPaymentType());
			transactions.setTransactionDesc(transactionsDto.getTransactionDescription());

			Float closeBalance = accountNumberPresent.get().getClosingBalance();

			if (transactionsDto.getPaymentType().equalsIgnoreCase(GlobalConstants.PAYMENT_TYPE)) {

				accountNumberPresent.get().setClosingBalance((float) (closeBalance + transactionsDto.getAmount()));

			} else {
				accountNumberPresent.get().setClosingBalance((float) (closeBalance - transactionsDto.getAmount()));
			}

			accountRepository.save(accountNumberPresent.get());

			transactionsRepository.save(transactions);

			ResponseDto responseDto = new ResponseDto();
			responseDto.setMessage(GlobalConstants.STATUS_MESSAGE);
			responseDto.setStatus(HttpStatus.OK.value());
			responseDto.setTransactionMessage(GlobalConstants.TRANSACTION_STATUS_MESSAGE);

			return Optional.of(responseDto);

		} else {
			throw new InsufficientFundException(GlobalConstants.INSUFFICIENT_FUND);
		}

	}

}
