package com.dto;

import java.util.List;

public class ErrorListResponseDto {
	private String statusCode;
	private List<String> statusMessage;

	public ErrorListResponseDto(String statusCode, List<String> statusMessage) {
		this.statusCode = statusCode;
		this.statusMessage = statusMessage;
	}

	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	public List<String> getStatusMessage() {
		return statusMessage;
	}

	public void setStatusMessage(List<String> statusMessage) {
		this.statusMessage = statusMessage;
	}
}
