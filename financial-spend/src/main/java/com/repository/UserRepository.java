package com.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.entity.User;

@Repository
public interface UserRepository extends CrudRepository<User, Integer> {

	User findByUsername(String username);

	Optional<List<User>> findAllById(Integer userId);

	Optional<User> findByAccountNumber(Long accountNumber);
}
