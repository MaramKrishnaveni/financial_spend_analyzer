package com.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.dto.TransactionsDto;
import com.exception.UserAccountNotFoundException;
import com.service.UserService;

/**
 * This controller is handling the requests and responses for user operations.
 * 
 * @author SQUAD 3
 * @since 2020/11/26
 */
@RestController
@RequestMapping("/users")
public class UserController {

	@Autowired
	UserService userService;

	/**
	 * Method to call service method for requesting to get past transactions history
	 * 
	 * @param accountNumber
	 * @return Optional<TransactionsDto> which consist of connection
	 *         userId,transactionDescription,amount,payment type,date
	 * 
	 */

	@GetMapping("/transactions")
	public ResponseEntity<Optional<List<TransactionsDto>>> showPastTransactions(
			@RequestParam @NotNull(message = "Account number required") Long accountNumber)
			throws UserAccountNotFoundException {
		return new ResponseEntity<>(userService.showAllTransactionData(accountNumber), HttpStatus.OK);
	}

}
