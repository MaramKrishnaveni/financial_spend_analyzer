package com.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.dto.UserDto;
import com.entity.User;
import com.service.UserService;

/**
 * This controller is handling the requests and responses for user registration.
 * 
 * @author SQUAD 3
 * @since 2020/11/26
 */
@RestController
public class RegistrationController {

	@Autowired
	private UserService userService;

	/**
	 * Method to call service method for requesting to save user
	 * 
	 * @RequestBody userDto
	 * @return Optional<User>
	 * 
	 */
	@PostMapping(value = "/signup")
	public ResponseEntity<User> signup(@Valid @RequestBody UserDto userDto) {
		User user = userService.save(userDto);
		return new ResponseEntity<>(user, HttpStatus.CREATED);
	}

}
