package com.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.dto.ResponseDto;
import com.dto.TransactionsDto;
import com.exception.InsufficientFundException;
import com.exception.UserAccountNotFoundException;
import com.service.TransactionService;

/**
 * This controller is handling the requests and responses for user to make 
 * transactions.
 * 
 * @author SQUAD 3
 * @since 2020/11/26
 */
@RestController
@RequestMapping("/api/transactions")
public class TransactionController {

	@Autowired
	TransactionService transactionService;

	/**
	 * Method to call service method for user to make transactions
	 * 
	 * @RequestBody transactionsDto
	 * @return Optional<ResponseDto> which consist of status message, status code,
	 *         Transaction message
	 * 
	 */
	@PostMapping("/transfer")
	public ResponseEntity<Optional<ResponseDto>> makeATransactions(@RequestBody TransactionsDto transactionsDto)
			throws InsufficientFundException, UserAccountNotFoundException {

		return new ResponseEntity<>(transactionService.makePaymentAndRecieve(transactionsDto), HttpStatus.OK);
	}

}
