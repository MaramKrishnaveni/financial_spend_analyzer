package com.exception;

public class NoTransactionHistoryException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4961344730456828654L;

	public NoTransactionHistoryException(String message) {
		super(message);
	}
}
