package com.service;

import java.util.Optional;

import com.dto.ResponseDto;
import com.dto.TransactionsDto;
import com.exception.InsufficientFundException;
import com.exception.UserAccountNotFoundException;

/**
 * TransactionMonthlyService
 * 
 * @author SQUAD 3
 * @since 2020/11/26
 */
public interface TransactionService {

	/**
	 * user to make transactions
	 * 
	 * @RequestBody transactionsDto
	 * @return Optional<ResponseDto> which consist of status message, status code,
	 *         Transaction message
	 */
	Optional<ResponseDto> makePaymentAndRecieve(TransactionsDto transactionsDto)
			throws InsufficientFundException, UserAccountNotFoundException;

}
